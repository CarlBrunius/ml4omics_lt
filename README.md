# ML4Omics_LT

This repository has presentation slides, scripts and permutation data to accompany the inaugural session of the Omics Working Group in LongITools (https://longitools.org), with a presentation and realtime analysis by me.

Associate Professor Carl Brunius  
carl.brunius@chalmers.se  
Department of Biology and Biological Engineering  
Chalmers University of technology  

If you're not used to R, RStudio and working in "projects", I suggest to Google some info on these procedures. You may also find my "MPCourse" package helpful (sorry for the slight disarray of files and folders, it's work in progress...):

https://gitlab.com/CarlBrunius/MPCourse

There, you'll find "A Super Brief R Tutorial", package installation instructions and some basic scipts / tasks to get you started in data analysis for Omics research

## Packages
I will use several packages, mine and others. I suggest run the following to install (hopefully) most of what I'll be using:

```
install.packages('remotes')
install.packages('pls')
install_gitlab('CarlBrunius/MPCourse')
install_gitlab('CarlBrunius/rdCV')
install_gitlab('CarlBrunius/MUVR')
```

You can find documents, tutorials and instructions at respective package's web repository:  
- https://gitlab.com/CarlBrunius/MUVR
- https://gitlab.com/CarlBrunius/rdCV
- https://gitlab.com/CarlBrunius/MPCourse

## Getting started

You can follow the presentation with the slides in the repo link.

There are 3 scripts that I will hopefully manage to go through in real-time. These should be "easily" modifiable (depending on your skill set of course) to suit your data and see if e.g. the MUVR approach is applicable to your particular data.

To save some time, there are also some permutation data that we'll use to assess model validity. 


